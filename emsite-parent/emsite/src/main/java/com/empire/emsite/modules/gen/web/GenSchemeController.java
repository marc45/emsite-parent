/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.modules.gen.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.gen.entity.GenScheme;
import com.empire.emsite.modules.gen.facade.GenSchemeFacadeService;
import com.empire.emsite.modules.gen.facade.GenTableFacadeService;
import com.empire.emsite.modules.sys.entity.User;
import com.empire.emsite.modules.sys.utils.UserUtils;

/**
 * 类GenSchemeController.java的实现描述：生成方案Controller
 * 
 * @author arron 2017年10月30日 下午7:15:33
 */
@Controller
@RequestMapping(value = "${adminPath}/gen/genScheme")
public class GenSchemeController extends BaseController {

    @Autowired
    private GenSchemeFacadeService genSchemeFacadeService;

    @Autowired
    private GenTableFacadeService  genTableFacadeService;

    @ModelAttribute
    public GenScheme get(@RequestParam(required = false) String id) {
        if (StringUtils.isNotBlank(id)) {
            return genSchemeFacadeService.get(id);
        } else {
            return new GenScheme();
        }
    }

    @RequiresPermissions("gen:genScheme:view")
    @RequestMapping(value = { "list", "" })
    public String list(GenScheme genScheme, HttpServletRequest request, HttpServletResponse response, Model model) {
        User user = UserUtils.getUser();
        if (!user.isAdmin()) {
            genScheme.setCreateBy(user);
        }
        Page<GenScheme> page = genSchemeFacadeService.find(new Page<GenScheme>(request, response), genScheme);
        model.addAttribute("page", page);

        return "modules/gen/genSchemeList";
    }

    @RequiresPermissions("gen:genScheme:view")
    @RequestMapping(value = "form")
    public String form(GenScheme genScheme, Model model) {
        if (StringUtils.isBlank(genScheme.getPackageName())) {
            genScheme.setPackageName("com.empire.emsite.modules");
        }
        //		if (StringUtils.isBlank(genScheme.getFunctionAuthor())){
        //			genScheme.setFunctionAuthor(UserUtils.getUser().getName());
        //		}
        model.addAttribute("genScheme", genScheme);
        model.addAttribute("config", genTableFacadeService.getGenConfig());
        model.addAttribute("tableList", genTableFacadeService.findAll());
        return "modules/gen/genSchemeForm";
    }

    @RequiresPermissions("gen:genScheme:edit")
    @RequestMapping(value = "save")
    public String save(GenScheme genScheme, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, genScheme)) {
            return form(genScheme, model);
        }
        genScheme.setCurrentUser(UserUtils.getUser());
        String result = genSchemeFacadeService.save(genScheme, UserUtils.getUser());
        addMessage(redirectAttributes, "操作生成方案'" + genScheme.getName() + "'成功<br/>" + result);
        return "redirect:" + adminPath + "/gen/genScheme/?repage";
    }

    @RequiresPermissions("gen:genScheme:edit")
    @RequestMapping(value = "delete")
    public String delete(GenScheme genScheme, RedirectAttributes redirectAttributes) {
        genSchemeFacadeService.delete(genScheme);
        addMessage(redirectAttributes, "删除生成方案成功");
        return "redirect:" + adminPath + "/gen/genScheme/?repage";
    }

}
