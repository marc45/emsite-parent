/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.test.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.empire.emsite.common.config.MainConfManager;
import com.empire.emsite.common.persistence.Page;
import com.empire.emsite.common.utils.StringUtils;
import com.empire.emsite.common.web.BaseController;
import com.empire.emsite.modules.sys.utils.UserUtils;
import com.empire.emsite.test.dto.TestDataDTO;
import com.empire.emsite.test.facade.TestDataFacadeService;
import com.unj.dubbotest.provider.DemoService;

/**
 * 类TestDataController.java的实现描述：单表生成Controller
 * 
 * @author arron 2017年10月30日 下午7:25:38
 */
@Controller
@RequestMapping(value = "${adminPath}/test/testData")
public class TestDataController extends BaseController {

    @Autowired
    private DemoService           demoService;

    @Autowired
    private TestDataFacadeService testDataFacadeService;

    @ModelAttribute
    public TestDataDTO get(@RequestParam(required = false) String id) {
        TestDataDTO entity = null;
        if (StringUtils.isNotBlank(id)) {
            entity = testDataFacadeService.get(id);
        }
        if (entity == null) {
            entity = new TestDataDTO();
        }
        return entity;
    }

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = { "list", "" })
    public String list(TestDataDTO testDataDTO, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<TestDataDTO> page = testDataFacadeService.findPage(new Page(request, response), testDataDTO);
        model.addAttribute("page", page);
        String hello = demoService.sayHello("guoxue");
        System.out.println(hello);
        //        List<TestData> x = testDataFacadeService.findList(testData);
        //        System.out.println("dubbo查询list" + x.size());
        //        List list = demoService.getUsers();
        //        if (list != null && list.size() > 0) {
        //            for (int i = 0; i < list.size(); i++) {
        //                System.out.println(list.get(i));
        //            }
        //        }
        return "emsite/test/testDataList";
    }

    @RequiresPermissions("test:testData:view")
    @RequestMapping(value = "form")
    public String form(TestDataDTO testDataDTO, Model model) {
        model.addAttribute("testDataDTO", testDataDTO);
        return "emsite/test/testDataForm";
    }

    @RequiresPermissions("test:testData:edit")
    @RequestMapping(value = "save")
    public String save(TestDataDTO testDataDTO, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, testDataDTO)) {
            return form(testDataDTO, model);
        }
        testDataDTO.setCurrentUser(UserUtils.getUser());
        //设置是否删除标记为正常
        testDataDTO.setDelFlag("0");
        testDataFacadeService.save(testDataDTO);
        addMessage(redirectAttributes, "保存单表成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/test/testData/?repage";
    }

    @RequiresPermissions("test:testData:edit")
    @RequestMapping(value = "delete")
    public String delete(TestDataDTO testDataDTO, RedirectAttributes redirectAttributes) {
        testDataFacadeService.delete(testDataDTO);
        addMessage(redirectAttributes, "删除单表成功");
        return "redirect:" + MainConfManager.getAdminPath() + "/test/testData/?repage";
    }

}
