/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.supcan.freeform;

import com.empire.emsite.common.supcan.common.Common;
import com.empire.emsite.common.supcan.common.properties.Properties;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 类FreeForm.java的实现描述：硕正FreeForm
 * 
 * @author arron 2017年10月30日 下午3:21:09
 */
@XStreamAlias("FreeForm")
public class FreeForm extends Common {

    public FreeForm() {
        super();
    }

    public FreeForm(Properties properties) {
        this();
        this.properties = properties;
    }

}
