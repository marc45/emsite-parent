/**
 * Copyright &copy; 2017 <a href="https://gitee.com/hackempire/emsite-parent">emsite</a> All rights reserved.
 */
package com.empire.emsite.common.supcan.annotation.common.properties;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类SupExpress.java的实现描述：硕正Express注解
 * 
 * @author arron 2017年10月30日 下午3:26:30
 */
@Target({ ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface SupExpress {

    /**
     * 是否自动按列的引用关系优化计算顺序 默认值true
     */
    String isOpt() default "";

    /**
     * 文本
     */
    String text() default "";

}
